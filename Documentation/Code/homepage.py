import eligorithm
from eligorithm import Estimation2
from bottle import route, error, post, get, run, static_file, abort, redirect, response, request, template, Request
from urllib.request import urlopen
from bottle import SimpleTemplate, BaseTemplate 
from bottle import Jinja2Template
import json

# Home Page deployment:
@route('/')
def homepageclick():
    return template('homepage')

@route('/admin')
def onclickadmin():
    return template('admin_login')
@route('/driver')
def onclickdriver():
    return template('driver_login' )

#Admin_login Page verified and deployed:
@post('/admin_homepage')
def adminhomepage():
    return template('admin_homepage')

#Admin_home Page:
@route('/logout')
def onclicklogout():
    return template('homepage')
@route('/logoutamb1')
def logoutamb1():
    return template('admin_finalpage')
@route('/logoutamb2')
def logoutamb2():
    return template('admin_finalpage')
@route('/logoutamb3')
def logoutamb3():
    return template('admin_finalpage')
@route('/logoutamb4')
def logoutamb4():
    return template('admin_finalpage')

#Admin_final Page:
@route('/adfinallogout')
def adfinallogout():
    return template('homepage')
@route('/adfinalhome')
def adfinalhome():
    return template('homepage')

#Driver_login Page verified and deployed
@post('/driver_login')
def driverhomepage():
    return template('driver_homepage')

#Driver_homepage:
@route('/drlogout')
def drlogout():
    return template('homepage')

@post('/submit')
def Estimation2():
     zone= request.forms.get('Zone')
     day= request.forms.get('Day')
     weather= request.forms.get('Weather')
     hour= request.forms.get('Hours')
     minute= request.forms.get('Minutes')
     urgency= request.forms.get('Urgency')
     ans = eligorithm.Estimation2(zone,day,weather,hour,minute,urgency)
     min = int(ans/60)
     secs = ans%60
     ans = str(min) + " minutes and " +str(secs) + " seconds"
     print(ans)
     return template('driver_finalpage', value=ans)

#Driver_finalpage:
@route('/clicksiren')
def clicksiren():
    return template('AfterSirenClick')

@route('/drfinalpagelogout')
def drfinalpagelogout():
    return template('homepage')

@route('/finalhome')
def finalhome():
    return template('homepage')

#AfterSirenClick page:
@route('/aftersirenlogout')
def aftersirenlogout():
    return template('homepage')

@route('/aftersirenhome')
def aftersirenhome():
    return template('homepage')

run(reloader=True)

 



















 
