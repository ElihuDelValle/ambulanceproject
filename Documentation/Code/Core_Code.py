import json
from urllib.request import urlopen
import time
import datetime
import pymongo
import re
import eligorithm
from eligorithm import Estimation
from bottle import route, error, post, get, run, static_file, abort, redirect, response, request, template, Request
from bottle import SimpleTemplate, BaseTemplate
from bottle import urlencode


'''@route('/')
def home():
   return template('driver_homepage')

@post('/submit')
def Estimation():
    zone= request.forms.get('Zone')
    day= request.forms.get('Day')
    weather= request.forms.get('Weather')
    hour= request.forms.get('Hours')
    minute= request.forms.get('Minutes')
    urgency= request.forms.get('Urgency')
    ans = eligorithm.Estimation(zone,day,weather,hour,minute,urgency)
    ans = str(ans) + " mins"
    print(ans)
    return template('driver_finalpage', value=ans)
run(reloader=True)'''


#Database Connectivity
client = pymongo.MongoClient("mongodb://infosonu:tester.12345@cluster0-shard-00-00-nylf1.mongodb.net:27017,cluster0-shard-00-01-nylf1.mongodb.net:27017,cluster0-shard-00-02-nylf1.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin")
db = client.test

collection = db["Data"]
data = {}

#Cleaning method to eliminate brackets 
def clean(text):
    coords = ''
    for x in text:
        if not(x == ' ' or x == '[' or x == "'" or x == ']'):
            coords = coords + x
    return coords
 
#Hospitals listing
       
def hospitals():
    hospitals = [None] * 13
    hospitals[0] = ['-37.6921694,145.0629215'],['Northpark Private Hospital']
    hospitals[1] = ['-37.7567201,144.9721956'],['Brunswick Private Hospital']
    hospitals[2] = ['-37.7995438,144.9565315'],['The Royal Melbourne Hospital']
    hospitals[3] = ['-37.8075801,144.976058'],["St Vincent's Private Hospital"]
    hospitals[4] = ['-37.845952,144.9818516'],['The Alfred']
    hospitals[5] = ['-37.7937111,144.9490643'],["The Royal Children's Hospital"]
    hospitals[6] = ['-37.652886,145.0144858'],['The Northern Hospital']
    hospitals[7] = ['-37.7545655,145.0477266'],['Heidelberg Repatriation Hospital']
    hospitals[8] = ['-37.7561333,145.0598074'],['Austin Hospital']
    hospitals[9] = ['-37.7167631,145.044365'],['La Trobe University Medical Centre']
    hospitals[10] = ['-37.7561681,145.0611484'],['Mercy Hospital For Women']
    hospitals[11] = ['-37.7543904,144.9584733'],['John Fawkner Private Hospital']
    hospitals[12] = ['-37.8549325,144.9985034'],['The Avenue Hospital']
    return hospitals

#Defining Origins [Coords][Suburb][Map Point]

def origins():
    origins = [None] * 14
    origins[0] = ['-37.74995, 145.05764'],['Heidelberg'],['1']
    origins[1] = ['-37.76433, 145.06038'],['Bundoora'],['2']
    origins[2] = ['-37.76976, 145.0439'],['Bundoora'],['3']
    origins[3] = ['-37.74805, 145.04133'],['Bundoora'],['4']
    origins[4] = ['-37.74275, 145.07411'],['Bundoora'],['5']
    origins[5] = ['-37.76949, 145.07978'],['Bundoora'],['6']
    origins[6] = ['-37.78916, 145.04631'],['Bundoora'],['7']
    origins[7] = ['-37.7661, 145.02107'],['Bundoora'],['8']
    origins[8] = ['-37.738, 145.09677'],['Bundoora'],['9']
    origins[9] = ['-37.78482, 145.08115'],['Bundoora'],['10']
    origins[10] = ['-37.72632, 145.04888'],['Bundoora'],['11']
    origins[11] = ['-37.74085, 145.02433'],['Bundoora'],['12']
    origins[12] = ['-37.72823, 145.08012'],['Bundoora'],['13']
    origins[13] = ['-37.76718, 145.10295'],['Bundoora'],['14']
    return origins

#Not used methods

'''def apiBuildManyToMany():

    apis = [None] * (len(origins())*len(hospitals()))
    x=0
    z=0
    googleAPI = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&departure_time=now&origins='
    apiKey = '&key=AIzaSyCxg5B-1sA0R6TeyvI-PRma0v7k56dDes8'
    googleAPI2 = '&destinations='
       
    while x<len(hospitals()):
        hospitalRaw = hospitals()[x][0]
        hospital = clean(hospitalRaw)
        y=0
        while y<len(origins()):
            originRaw = origins()[y][0]
            origin = clean(originRaw)
            finalAPI = googleAPI + origin + googleAPI2 + hospital + apiKey
            apis[z] = finalAPI
            y += 1
            z += 1
        x += 1
    return apis

# From only one source to all hospitals'''

'''def apiBuild1toM(hospital):
    x=0
    y=0
    coords = hospitals()[hospital][0]
    size = 2*(len(origins()))
    apis = [None] * size
    hospital = clean(str(coords))
    googleAPI = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&departure_time=now&origins='
    apiKey = '&key=AIzaSyCxg5B-1sA0R6TeyvI-PRma0v7k56dDes8'
    googleAPI2 = '&destinations='
       
    while (x<(size/2)):
        originRaw = origins()[x][0]
        origin = clean(originRaw)
        finalAPI = googleAPI + origin + googleAPI2 + hospital + apiKey
        apis[y] = finalAPI
        y += 1
        finalAPI = googleAPI + hospital + googleAPI2 + origin + apiKey
        apis[y] = finalAPI
        y += 1
        x += 1
    return apis'''


#API Builders

def apiBuildSingle(source,destination,key):
    key = '&key=' + key
    origin = origins()[source][0]
    dest = hospitals()[destination][0]
    origin = clean(str(origin))
    dest = clean(str(dest))
    googleAPI = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&departure_time=now&origins='
    googleAPI2 = '&destinations='
    
    finalAPI = googleAPI + origin + googleAPI2 + dest + key
    return finalAPI

def apiBuildSingleInvert(source,destination,key):
    key = '&key=' + key
    origin = origins()[source][0]
    dest = hospitals()[destination][0]
    origin = clean(str(origin))
    dest = clean(str(dest))
    googleAPI = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&departure_time=now&origins='
    googleAPI2 = '&destinations='
    
    finalAPI = googleAPI + dest + googleAPI2 + origin + key
    return finalAPI

#Google Maps Matric info extract

def information(api):
    
    data = json.load(urlopen(api))
    #status = data['status']
    #Getting Address
    destAddress = data['destination_addresses'][0]
    originAddress = data['origin_addresses'][0]
    #addressStr = 'Origin: ' + originAddress + '\nDestination: ' + destAddress
    #Getting Duration
    durationRaw = data['rows'][0]['elements'][0]['duration_in_traffic']['value']
    durationMin = float((durationRaw)/60 - ((durationRaw%60)/100))
    durationSec = float(durationRaw)%60
    durationStr = (str("{:.0f}".format(durationMin)) + ':' + str("{:.0f}".format(durationSec)))
    #Getting Distance
    distanceRaw = data['rows'][0]['elements'][0]['distance']['value']
    distance = float(distanceRaw)/1000
    distanceStr = (str(distance))
    #Time Stamp
    timeSystem = time.time()
    timeNow = datetime.datetime.fromtimestamp(timeSystem).strftime('%Y-%m-%d %H:%M:%S')
    #Final Result
    finalData = [originAddress],[destAddress],[durationStr],[distanceStr],[timeNow]
    
    return finalData

# Get Functions from data

def getOrigin(data):
    string = ''
    origin = data[0]
    for x in range(0,len(origin)):
        if not x < 3 or x >= (len(origin) -2):
            string += origin[x]    
    return string

def getDestination(data):
    string = ''
    destination = data[1]
    for x in range(0,len(destination)):
        if not x < 3 or x >= (len(destination) -2):
            string += destination[x]    
    return string

def getDuration(data):
    string = ''
    destination = data[2]
    for x in range(0,len(destination)):
        if not x < 3 or x >= (len(destination) -2):
            string += destination[x]    
    return string

def getDistance(data):
    string = ''
    destination = data[3]
    for x in range(0,len(destination)):
        if not x < 3 or x >= (len(destination) -2):
            string += destination[x]    
    return string

def getTime(data):
    string = ''
    destination = data[4]
    for x in range(0,len(destination)):
        if not x < 3 or x >= (len(destination) -2):
            string += destination[x]    
    return string

def getSuburb(x):
    suburb = str(origins()[x][1])
    string = ''
    for x in range(0,len(suburb)-2):
        if not x < 2:
            string += suburb[x]    
    return string

def getZone(x):
    suburb = str(origins()[x][2])
    string = ''
    for x in range(0,len(suburb)-2):
        if not x < 2:
            string += suburb[x]    
    return string

def getWeekDay(number):
    if(number == 1):
        return 'Tuesday'
    elif(number == 2):
        return 'Wednesday'
    elif(number == 3):
        return 'Thursday'
    elif(number == 4):
        return 'Friday'
    elif(number == 5):
        return 'Saturday'
    elif(number == 6):
        return 'Sunday'
    elif(number == 0):
        return 'Monday'
    
def getHospital(x):
    name = str(hospitals()[x][1])
    string = ''
    for x in range(0,len(name)-2):
        if not x < 2:
            string += name[x]    
    return string
#Weather API

def getWeather(source):
    api = 'https://api.darksky.net/forecast/2e6289c2324b2b98e6ba8ae0a190c1d9/'
    coords = clean(str(origins()[source][0]))
    finalAPI = api + coords
    data = json.load(urlopen(finalAPI))
    weather = data['currently']['summary']
    return weather
    
'''def test(apiList,hospital):
    count = 1
    for x in apiList:
        data = information(x)
        if not count%2 == 0:
            print('Origin:' + getOrigin(data))
            #print('Destination:' + getDestination(data))
            print('Hospital:' + getHospital(hospital))
            print('Duration:' + getDuration(data))
            print('Distance:' + getDistance(data))
            print('Time:' + getTime(data))
            print('-----------------------------')
        else:
            print('Road back: ')
            print('Duration:' + getDuration(data))
            print('Distance:' + getDistance(data))
            print('Time:' + getTime(data))
            print('-----------------------------')
        count += 1'''

#Runner , connects to mongo and stored data and creates also text file		
def run(api,api2,source,dest,fileName):
    with open(fileName, "w") as f:
        info = information(api) 
        info2 = information(api2)
        f.write('Origin: ' + getOrigin(info) + '\n')
        f.write('Zone: ' + getZone(source) + '\n')
        f.write('Hospital: ' + getHospital(dest) + '\n')
        f.write('Distance: ' + getDistance(info) + 'kms\n')
        f.write('-----------------------------\n')
        f.write('Delivery to Hospital|Pick-up|Zone|Day of the week|Weather|Time \n')
        x = 4
        e = 1
        while True: 
            try:            
                if x%4 == 0:
                    weather = str(getWeather(source))
                    x = 4
                x += 1
                #Splitting minutes and seconds
                dur = (getDuration(info))
                dur_split = re.split(':',dur) 
                minutes = dur_split[0]
                seconds = dur_split[1]
                dur2 = (getDuration(info2))
                dur2_split = re.split(':',dur2) 
                min2 = dur2_split[0]
                sec2 = dur2_split[1] 
                #Getting Zone and weekday             
                zone = (getZone(source))
                weekday = datetime.datetime.today().weekday() 
                weekday = getWeekDay(weekday)
                #Splitting time 
                time_raw = (getTime(info))
                time_split = re.split(r'( +|:+|-)',time_raw) 
                db.data.insert_one(
                {"Delivery Time": {
                    "Distance": float(getDistance(info)),
                    "Minutes": int(minutes),
                    "Seconds": int(seconds),
                    "Total Seconds": ((int(minutes)*60)+int(seconds))
                    },
                 "Pick up Time": {
                    "Distance": float(getDistance(info2)),
                    "Minutes": int(min2),
                    "Seconds": int(sec2),
                    "Total Seconds": ((int(min2)*60)+int(sec2))
                     },
                 "Zone": zone,
                 "Day": weekday,
                 "Weather": weather,
                 "Time": {
                     "Year":int(time_split[0]),
                     "Month":int(time_split[2]),
                     "Day":int(time_split[4]),
                     "Hour":int(time_split[6]),
                     "Minute":int(time_split[8]),
                     "Second":int(time_split[10])
                     }
                 })     
                f.write(dur + '|' + dur2  + '|' + zone + '|' + weekday + '|' + weather + '|' + time_raw + '|' + '\n')
                f.flush()
                time.sleep(300)
                info = information(api)
                info2 = information(api2)
            except Exception as error:
                f.write(str(e) +"errors" + str(error) +"\n")
                e += 1
            finally:
                f.flush()            