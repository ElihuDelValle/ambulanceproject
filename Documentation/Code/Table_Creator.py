import pymongo
import xlrd
import csv
import xlsxwriter

client = pymongo.MongoClient("mongodb://infosonu:tester.12345@cluster0-shard-00-00-nylf1.mongodb.net:27017,cluster0-shard-00-01-nylf1.mongodb.net:27017,cluster0-shard-00-02-nylf1.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin")
db = client.test

def transformMinute(minute):
    if(minute > 30):
        return 2
    else:
        return 1
    
def transformDay(day):
    if(day == 'Monday'):
        return 1
    elif(day == 'Tuesday'):
        return 2
    elif(day == 'Wednesday'):
        return 3
    elif(day == 'Thursday'):
        return 4
    elif(day == 'Friday'):
        return 5
    elif(day == 'Saturday'):
        return 6
    else:
        return 7
    
def transformWeather(weather):
    if(weather == 'Clear'):
        return 1
    elif(weather == 'Drizzle'):
        return 2
    elif(weather == 'Dry and Mostly Cloudy'):
        return 3
    elif(weather == 'Dry and Overcast'):
        return 4
    elif(weather == 'Dry and Partly Cloudy'):
        return 5
    elif(weather == 'Light Rain'):
        return 6
    elif(weather == 'Mostly Cloudy'):
        return 7
    elif(weather == 'Overcast'):
        return 8
    elif(weather == 'Partly Cloudy'):
        return 9
    else:
        return 10

def createTableFull():
    x = 0
    excel = xlsxwriter.Workbook('Excel.xlsx')
    writer = excel.add_worksheet('Full Data')
    '''writer.write(0,0,'Zone')
    writer.write(0,1,'Day')
    writer.write(0,2,'Weather')
    writer.write(0,3,'Hour')
    writer.write(0,4,'Minute')
    writer.write(0,5,'ETA')
    writer.write(0,6,'urgency')'''
    cursor = db.data.find({})
    for doc in cursor: 
        #print(doc)
        preMinute = doc["Time"]["Minute"]
        preDay = doc["Day"]
        preWeather = doc["Weather"]
        hour = doc["Time"]["Hour"]
        zone = doc["Zone"]
        minute = transformMinute(preMinute)
        day = transformDay(preDay)
        weather = transformWeather(preWeather)            
        time = doc["Pick up Time"]["Total Seconds"]
        time = time/60
        time = round(time)
        #-----------------------------------------------
        writer.write(x,0,zone)
        writer.write(x,1,day)
        writer.write(x,2,weather)
        writer.write(x,3,hour)
        writer.write(x,4,minute)  
        writer.write(x,5,"1")
        writer.write(x,6,time)
        x += 1
        #-----------------------------------------------
        time2 = time-(time*(0.05))
        time2 = round(time2)
        writer.write(x,0,zone)
        writer.write(x,1,day)
        writer.write(x,2,weather)
        writer.write(x,3,hour)
        writer.write(x,4,minute) 
        writer.write(x,5,"2")
        writer.write(x,6,time2)      
        x += 1
        #-----------------------------------------------
        time3 = time-(time*(0.15))
        time3 = round(time3)
        writer.write(x,0,zone)
        writer.write(x,1,day)
        writer.write(x,2,weather)
        writer.write(x,3,hour)
        writer.write(x,4,minute) 
        writer.write(x,5,"3")
        writer.write(x,6,time3)
        x += 1
        #-----------------------------------------------
    excel.close()  
    csv_from_excel() 

'''def createTableInput(zone,day,weather,hour,minute,fileName):
    x = 1
    excel = xlsxwriter.Workbook(fileName)
    writer = excel.add_worksheet('Full Data')
    writer.write(0,0,'Zone')
    writer.write(0,1,'Day')
    writer.write(0,2,'Weather')
    writer.write(0,3,'Hour')
    writer.write(0,4,'Minute')
    writer.write(0,5,'ETA')
    writer.write(0,6,'Urgency')
    cursor = db.data.find({"Zone":zone,"Day":day,"Time.Hour":hour,"Time.Minute":minute})
    for doc in cursor: 
        #print(doc)
        time = doc["Pick up Time"]["Total Seconds"]
        time = round(time)
        writer.write(x,0,doc["Zone"])
        writer.write(x,1,doc["Day"])
        writer.write(x,2,doc["Weather"])
        writer.write(x,3,doc["Time"]["Hour"])
        writer.write(x,4,doc["Time"]["Minute"])
        writer.write(x,5,time)
        writer.write(x,6,"Low")
        x += 1
        time2 = time-(time*(0.05))
        time2 = round(time2)
        print(time2)
        writer.write(x,0,doc["Zone"])
        writer.write(x,1,doc["Day"])
        writer.write(x,2,doc["Weather"])
        writer.write(x,3,doc["Time"]["Hour"])
        writer.write(x,4,doc["Time"]["Minute"])
        writer.write(x,5,time2)
        writer.write(x,6,"Medium")
        x += 1
        time3 = time-(time*(0.15))
        time3 = round(time3)
        writer.write(x,0,doc["Zone"])
        writer.write(x,1,doc["Day"])
        writer.write(x,2,doc["Weather"])
        writer.write(x,3,doc["Time"]["Hour"])
        writer.write(x,4,doc["Time"]["Minute"])
        writer.write(x,5,time3)
        writer.write(x,6,"High")
        x += 1
    excel.close()
'''

def createTableManual(zone,day,weather,hour,minute,urgency):
    excel = xlsxwriter.Workbook('Query.xlsx')
    writer = excel.add_worksheet('Full Data')

    preMinute = minute
    preDay = day
    preWeather = weather
    hour = hour
    zone = zone
    minute = transformMinute(preMinute)
    #print("hello")
    #print(type(minute))
    #print(minute)
    #minute = transformMinute(minute)
    day = transformDay(preDay)
    weather = transformWeather(preWeather)            
    #-----------------------------------------------
    writer.write(0,0,zone)
    writer.write(0,1,day)
    writer.write(0,2,weather)
    writer.write(0,3,hour)
    writer.write(0,4,minute)  
    writer.write(0,5,urgency)
    #-----------------------------------------------
    excel.close()   
    #-----------------------------------------------
    csv_from_excelB('Query.xlsx', 'Experiment.csv')
        
def csv_from_excel():
    wb = xlrd.open_workbook('Excel.xlsx')
    sh = wb.sheet_by_name('Full Data')
    your_csv_file = open('Full_Data.csv', 'w')
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

    for rownum in range(sh.nrows):
        wr.writerow(sh.row_values(rownum))

    your_csv_file.close()
    
def csv_from_excelB(inputF,outputF):
    wb = xlrd.open_workbook(inputF)
    sh = wb.sheet_by_name('Full Data')
    your_csv_file = open(outputF, 'w')
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

    for rownum in range(sh.nrows):
        wr.writerow(sh.row_values(rownum))

    your_csv_file.close()

createTableManual(1, 'Tuesday', 'Mostly Cloudy', 12, 10, 1)
#createTableFull()
