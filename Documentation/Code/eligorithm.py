import pandas
#from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
import Table_Creator



def Estimation2(zone,day,weather,hour,minute,urgency):
    fileName = 'Full_Data.csv'
    #url = 'C:\Users\elihu\eclipse-workspace\DataMiner\Predictive_Algorithm\Full_Data.csv'
    names = ['Zone', 'Day', 'Weather', 'Hour', 'Minute','Urgency','ETA']
    dataset = pandas.read_csv(fileName,names=names)
    #print(dataset)
    
    #print(dataset.shape)
    #print(dataset.describe())
    #dataset.plot(kind='box', subplots=True, layout=(3,3), sharex=False, sharey=False)
    #plt.show()
    #dataset.hist()
    #plt.show()
    array = dataset.values
    X = array[:,0:6]
    Y = array[:,6]
    validation_size = 0.20
    seed = 7
    X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size, random_state=seed)
    #Stuff
    scoring = 'accuracy'
    #print(dataset.groupby('ETA').size())
    # Spot Check Algorithms
    
    models = []
    #models.append(('LR', LogisticRegression()))
    #models.append(('LDA', LinearDiscriminantAnalysis()))
    #models.append(('KNN', KNeighborsClassifier()))
    models.append(('CART', DecisionTreeClassifier()))
    #models.append(('NB', GaussianNB()))
    #models.append(('SVM', SVC()))
    # evaluate each model in turn
    results = []
    names = []
    for name, model in models:
        kfold = model_selection.KFold(n_splits=10, random_state=seed)
        cv_results = model_selection.cross_val_score(model, X_train, Y_train, cv=kfold, scoring=scoring)
        results.append(cv_results)
        names.append(name)
        msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
        #print(msg)
        
    # Make predictions on validation dataset
    cart = DecisionTreeClassifier()
    cart.fit(X_train, Y_train)
    #print(X_validation)
    predictions = cart.predict(X_validation)
    #print(predictions)
    #print(accuracy_score(Y_validation, predictions))
    #print(confusion_matrix(Y_validation, predictions))
    #print(classification_report(Y_validation, predictions))
    #---------------------------------------------------------------------
    zoneT = zone
    dayT = day
    weatherT = weather
    hourT = int(hour)
    minuteT = int(minute)
    urgencyT = urgency
    Table_Creator.createTableManual(zoneT,dayT,weatherT,hourT,minuteT,urgencyT)
    
    fileName = 'Experiment.csv'
    names = ['Zone', 'Day', 'Weather', 'Hour', 'Minute','Urgency','ETA']
    dataset = pandas.read_csv(fileName,names=names)
    array = dataset.values
    X = array[:,0:6]
    Y = array[:,6]
    validation_size = 0.20
    seed = 7
    X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size, random_state=seed)
    newP = cart.predict(X_validation)
    result = int(newP)
    #print('The Ambulance will arrive in : ' + str(result*60) + 'seconds')
    return result*60

    
