
<html>
<head>
   <title>Ambulance</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
table, th , td {

font-family: 'Roboto', sans-serif;

font-size:14px;
  border: 1px solid #136189;
  text-align: center;
  border-collapse: collapse;
  padding: 5px;
  color:#FFFFFF;
  background-color: #EDE9E3 ;

}

table {
    
    width: 40%;
	<!-- overflow: visible; -->
}
th,td {
    
	width: 9%;
	text-align: center;
	
}
td{
 border: 0px ;
}

</style>
<body>

<center>
<br></br>
<br></br>
<br></br>
<table style="width:45%">
  <tr>
    <th style='background-color:#136189;'><center>Ambulance</center></th>
  </tr>
  <tr>
    <td><br></br><br></br><br></br><button onclick="location.href='/admin'" type="button" class="btn btn-primary"><i class="fa fa-user"></i>&nbsp;Admin</button></td>
   </tr>
  <tr>
   <td><br></br><button onclick="location.href='/driver'" type="button" class="btn btn-primary"><i class="fa fa-car"></i>&nbsp;Driver</button><br></br>
   <br></br><br></br><br></br>
   </td>
   </tr>
  
</table>

</center>
</body>
</html>
